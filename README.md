# 控制麦克风的音量

#### 介绍
用PYTHON写的控制麦克风的音量，没什么用，如果被其它软件控制了麦克风的音量，就用他锁定吧。

#### 软件架构
软件架构说明



#### 使用说明

1.  安装Python。
2.  把需要的库下载下来。
3.  运行我的这个python脚本啊，也就一个脚本，随便都能看见。

#### 其他
因为我自用嘛，哪天心情好把它打包成exe程序啊，再加个图形化界面吧。

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
